/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Servo Functions
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling servos on Pin 9 and 10 on the Arduino Due.
 *
 *****************************************************************************************/
#include "asf.h"
#include "stdio_serial.h"
#include "pwm.h"

/*****************************************************************************************
 * Local headerfiles
 *****************************************************************************************/
#include "mapper.h"
#include "servo.h"

/** \brief The PWM channel 4 Low for the servo handling at Arduino DUE Pin 9*/
static pwm_channel_t pwm_channel_instance; 

/** \brief The TC2 Channel 2 number of ticks for each period */
static uint32_t tc_ticks;

/**
 * \brief Set up the servo functionality on Pin 9 and 10 on the Arduin DUE.
 *
 * Sets up the servos at pin 9 and pin 10 at Arduino DUE. PWM Channel 4 is driving pin 9
 * and TC2 Channel 2 is driving pin 10.
 *
 * \returns Zero on success or error code on failure.
 */
uint32_t configure_servo (void)
{
  uint32_t ret = 0;

  message (MSG_INFO, "SERVO: Setup and configuration");
  /*
   * PWM Channel 4 - Due Pin 9 setup
   **/
  ioport_set_port_mode(IOPORT_PIOC, PIO_PC21B_PWML4, IOPORT_MODE_MUX_B);
  ioport_disable_port(IOPORT_PIOC, PIO_PC21B_PWML4);
  
  /* Enable the peripheral clock */
  message (MSG_INFO, "SERVO: Enable peripheral clock for PWM");
  ret = pmc_enable_periph_clk(ID_PWM);
  if (ret != 0) {
    message_value (MSG_ERROR, "SERVO: pmc_enable_periph_clk failed ", ret);
    return ERROR_SERVO_PWM_INIT_FAIL;
  }
  
  /* Disable the channel during configuration */
  message (MSG_INFO, "SERVO: Disable PWM channel 4 during configuration");
  pwm_channel_disable(PWM, PWM_CHANNEL_4);

  /* Set up the clock */
  pwm_clock_t clock_setting = {
    .ul_clka = SERVO_FREQUENCY * SERVO_PWM_RESOLUTION,
    .ul_clkb = 0,
    .ul_mck = sysclk_get_cpu_hz()
  };

  /* Init the PWM module */
  message (MSG_INFO, "SERVO: Initializing PWM");
  ret = pwm_init(PWM, &clock_setting);
  if (ret != 0) {
    message_value (MSG_ERROR, "SERVO: pwm_init failed ", ret);
    return ERROR_SERVO_PWM_INIT_FAIL;
  }
  
  /* Set up the PWM channel */
  pwm_channel_instance.ul_prescaler = PWM_CMR_CPRE_CLKA;
  pwm_channel_instance.ul_period = SERVO_PWM_RESOLUTION;
  pwm_channel_instance.ul_duty = ((SERVO_PWM_RESOLUTION*3)/40);
  pwm_channel_instance.polarity = PWM_LOW;
  pwm_channel_instance.alignment = PWM_ALIGN_LEFT;
  pwm_channel_instance.channel = PWM_CHANNEL_4;

  /* Init the channel */
  message (MSG_INFO, "SERVO: Initialize PWM channel 4");
  ret = pwm_channel_init(PWM, &pwm_channel_instance);
  if (ret != 0) {
    message_value (MSG_ERROR, "SERVO: pwm_channel_init PWM channel 4 failed ", ret);
    return ERROR_SERVO_PWM_INIT_FAIL;
  }

  /* Enable the channel */
  message (MSG_INFO, "SERVO: Enable PWM channel 4");
  pwm_channel_enable (PWM, PWM_CHANNEL_4);

  /*
   * TC2 Channel 2 (TIOB7) - Arduino Due pin 10 setup
   *
   * Set PC29 to peripheral B for TC2 Channel 2
   *
   * Set PA28 as input, we do not want it to interfere with PC29 since they are wired together
   * for arduino due pin 10 for some reason.
   *
   **/  
  ioport_set_port_mode(IOPORT_PIOC, PIO_PC29B_TIOB7, IOPORT_MODE_MUX_B);
  ioport_disable_port(IOPORT_PIOC, PIO_PC29B_TIOB7);
  
  ioport_set_port_dir (IOPORT_PIOA, PIO_PA28, IOPORT_DIR_INPUT);
  ioport_enable_port(IOPORT_PIOA, PIO_PA28);
  
  /* Start TC2 channel 2, i.e. ID_TC7*/
  message (MSG_INFO, "SERVO: Enable peripheral clock for TC7");
  pmc_set_writeprotect(false);
  ret = pmc_enable_periph_clk (ID_TC7);
  if (ret != 0) {
    message_value (MSG_ERROR, "pmc_enable_periph_clk TC7 failed ", ret);
    return ERROR_SERVO_PWM_INIT_FAIL;    
  }
  
  /* Configure TC2 channel 2 frequency and wave generation UP RC */
  uint32_t ul_div;
  uint32_t ul_tcclks;
  uint32_t ul_sysclk = sysclk_get_cpu_hz();

  message (MSG_INFO, "SERVO: Finding master clock divisor for TC7");
  ret = tc_find_mck_divisor(SERVO_FREQUENCY, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
  if (ret == 0) {
    message_value (MSG_ERROR, "SERVO: tc_find_mck_divisor failed ", ret);
    return ERROR_SERVO_TC_INIT_FAIL;        
  }
  message (MSG_INFO, "SERVO: Initialize TC7");
  tc_init(TC2, 1,   ul_tcclks | TC_CMR_WAVE | TC_CMR_BCPB_SET | TC_CMR_BCPC_CLEAR | TC_CMR_WAVSEL_UP_RC| TC_CMR_EEVT_XC0);

  /* Set up the SERVO_FREQUENCY */
  tc_ticks = (ul_sysclk/ul_div)/SERVO_FREQUENCY;
  tc_write_rc(TC2, 1, tc_ticks);

  /* Set up default middle value of servo 1.5 ms */
  tc_write_rb(TC2, 1, tc_ticks - (tc_ticks*3)/40);
  tc_start (TC2, 1);
  
  /* All went well */
  message (MSG_INFO, "SERVO: Setup and configuration completed");
  return SERVO_OK;
}

/**
 * \brief Sets the servo position for a specific servo by specifying pulse width.
 *
 * The servo high pulse width is typically between 500ms and 2500ms within a 20000ms
 * long pulse.
 *
 * \param [in] u - Pulse width in milliseconds
 *
 **/
uint32_t set_servo_position (enum servo id, uint32_t u)

{
  /* Decide what to do, based on servo */
  switch (id) {

    /* The PWM Pin 9 Servo */
  case SERVO_1:
    pwm_channel_update_duty (PWM, &pwm_channel_instance, (u * SERVO_PWM_RESOLUTION) / SERVO_PULSE_WIDTH);
    break;

    /* The TC Pin 10 Servo */
  case SERVO_2:
    tc_write_rb(TC2, 1, tc_ticks - u * (tc_ticks) / SERVO_PULSE_WIDTH);
    break;
  }
  
  return SERVO_OK;
}
