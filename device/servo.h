/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Servo Functions Header file
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling servos on Pin 9 and 10 on the Arduino Due.
 *
 *****************************************************************************************/
#ifndef _SERVO_H_
#define _SERVO_H_

/*****************************************************************************************
 * Errors
 *****************************************************************************************/

#define SERVO_OK 0 /**< Servo success */
#define ERROR_SERVO_PWM_INIT_FAIL 100 /**< PWM Initialization failure */
#define ERROR_SERVO_TC_INIT_FAIL  101 /**< TC Initialization failure */

/*****************************************************************************************
 * Servo constants
 *****************************************************************************************/

#define SERVO_FREQUENCY 50 /**< Servo control signal frequenc 50 Hzy, 20 ms period */

/**
 * \brief Servo control signal PWM resolution
 *
 * We are only using 0.5-2.5ms of the 20ms (50 hz) period for a full servo control signal. If we want a
 * resolution of 1000 ticks for that length of 2 ms, we need a PWM resolution of 
 * 1000 ticks * (1000ms/50Hz) / 2ms = 10000 ticks for the entire pulse length.
 *
 **/
#define SERVO_PWM_RESOLUTION 10000

/**
 * \brief Service total pulse width
 *
 * The servo total pulse width is 20000ms for this implementation. The high pulse width varies between
 * 500ms and 2000ms but can be set outside those bounds if needed. Make sure you do not break the
 * servo.
 *
 **/
#define SERVO_PULSE_WIDTH 20000

/**
 * \brief Servo identities.
 *
 * The identity of each servo to be used when setting the servo position.
 **/
enum servo {SERVO_1=0   /**< Arduino Due Pin 9 Servo */
	   , SERVO_2=2  /**< Arduino Due Pin 10 Servo */
           };

/*****************************************************************************************
 * Function prototypes
 *****************************************************************************************/
extern uint32_t configure_servo(void);
extern uint32_t set_servo_position (enum servo id, uint32_t);

#endif
