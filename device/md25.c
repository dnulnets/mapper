/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Servo Functions
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling the MD25 motor controller
 *
 *****************************************************************************************/
#include "asf.h"
#include "stdio_serial.h"

/*****************************************************************************************
 * Local headerfiles
 *****************************************************************************************/
#include "mapper.h"
#include "md25.h"

