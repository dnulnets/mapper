/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Sonar SRF10 Functions Header file
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling the SRF10 Sonar
 *
 *****************************************************************************************/
#ifndef _SRF10_H_
#define _SRF10_H_

/*****************************************************************************************
 * Errors
 *****************************************************************************************/

#define SRF10_OK 0 /**< The SRF10 sensor is ok */
#define ERROR_SRF10_PROBE_FAILED 100 /**< The SRF10 sensor is not detected on the I2C bus */
#define ERROR_SRF10_COMMUNICATION_FAILED 101 /**< The SRF10 sensor is not possible to communicate with */
#define ERROR_SRF10_MEASURE_FAILED_INIT 102 /**< The SRF10 sensor measurement reading could not be started */
#define ERROR_SRF10_MEASURE_READING_FAILED 103 /**< The SRF10 sensor reading failed */

/*****************************************************************************************
 * SRF10 Sonar constants
 *****************************************************************************************/

#define SRF10_CONFIG_ADDRESS 113 /**< The I2C address to the SRF10 sonar **/
#define SRF10_CONFIG_RANGE_WAIT 100 /**< The maximum number of milliseconds to wait for a measurement to be finished */
#define SRF10_CONFIG_MAX_GAIN 8 /**< Maximum gain 140 */
#define SRF10_CONFIG_MAX_RANGE 255 /**< Maximum range, 255 * 43mm + 43mm = 11008mm */

/**
 * Read registers on the SRF10 sonar
 **/
#define SRF10_SOFTWARE_REVISION 0 /**< The SRF10 Software revision register **/
#define SRF10_RANGE 2 /**< The 2 byte MSB, LSB of the SRF10 Range register */

/**
 * Write registers on the SRF10 sonar
 **/
#define SRF10_COMMAND 0 /**< The SRF10 Command register **/
#define SRF10_MAX_GAIN 1 /**< The SRF10 Max gain register (0-16) **/
#define SRF10_MAX_RANGE 2 /**< The SRF10 Max range register (0-255) **/

/**
 * SRF10 Commands
 **/
#define SRF10_COMMAND_RANGE_MODE_INCHES 80 /**< The SRF10 Ranging mode for result in inches */
#define SRF10_COMMAND_RANGE_MODE_CM 81 /**< The SRF10 Ranging mode for result in centimeters */
#define SRF10_COMMAND_RANGE_MODE_uS 82 /**< The SRF10 Ranging mode for result in microseconds */

/*****************************************************************************************
 * Function prototypes
 *****************************************************************************************/
extern uint32_t config_srf10 (void);
extern uint32_t srf10_measure (uint32_t *distance);

#endif
