/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Servo Functions
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling the MD25 motor controller
 *
 *****************************************************************************************/
#include "asf.h"
#include "stdio_serial.h"

/*****************************************************************************************
 * Local headerfiles
 *****************************************************************************************/
#include "mapper.h"
#include "srf10.h"
#include "base/i2c.h"

/**
 * \brief Sets up and configures the SRF10 Sonar.
 *
 * \returns SRF10_OK if succesful
 **/
uint32_t config_srf10 (void)
{
  message (MSG_INFO, "SRF10: Setup and configuration");

  /* Probe for the device */
  message (MSG_INFO, "SRF10: Probing bus for device");
  if (i2c_test (SRF10_CONFIG_ADDRESS) != I2C_OK) {
    message (MSG_ERROR, "SRF10: Device not detected");
    return ERROR_SRF10_PROBE_FAILED;
  }
  message (MSG_INFO, "SRF10: Detected device");

  /* Check the software */
  uint8_t revision;
  if (i2c_read_byte (SRF10_CONFIG_ADDRESS, SRF10_SOFTWARE_REVISION, &revision) != I2C_OK) {
    message (MSG_ERROR, "SRF10: Unable to retrieve software version");
    return ERROR_SRF10_COMMUNICATION_FAILED;
  }
  message_value (MSG_INFO, "SRF10: Software revision ", (uint32_t)revision);

  /* Set maximum gain */
  message_value (MSG_INFO, "SRF10: Setting maximum gain to ", SRF10_CONFIG_MAX_GAIN);
  if (i2c_write_byte (SRF10_CONFIG_ADDRESS, SRF10_MAX_GAIN, SRF10_CONFIG_MAX_GAIN) != I2C_OK) {
    message (MSG_ERROR, "SRF10: Unable to set maximum gain");
    return ERROR_SRF10_COMMUNICATION_FAILED;
  }

  /* Set maximum range */
  message_value (MSG_INFO, "SRF10: Setting maximum range to ", SRF10_CONFIG_MAX_RANGE);
  if (i2c_write_byte (SRF10_CONFIG_ADDRESS, SRF10_MAX_RANGE, SRF10_CONFIG_MAX_RANGE) != I2C_OK) {
    message (MSG_ERROR, "SRF10: Unable to set maximum range");
    return ERROR_SRF10_COMMUNICATION_FAILED;
  }

  /* All went well */
  message (MSG_INFO, "SRF10: Setup and configuration completed");
  return SRF10_OK;
}

/**
 * \brief Measures the distance of the echo in centimeters.
 *
 * The function starts a measurement of the distance in centimeters and returns with the
 * result.
 * 
 * \param [out] distance The distance in centimeters
 * \returns TWI_SUCCESS if everything went okey
 **/
uint32_t srf10_measure (uint32_t *distance)
{
  *distance = 0;
  
  /* Start the measurement */
  if (i2c_write_byte (SRF10_CONFIG_ADDRESS, SRF10_COMMAND, SRF10_COMMAND_RANGE_MODE_CM) != SRF10_OK) {
    return ERROR_SRF10_MEASURE_FAILED_INIT;
  }

  /* Wait for a couple of milliseconds */
  mdelay (SRF10_CONFIG_RANGE_WAIT);

  /* Read the value */
  uint8_t value[2];
  if (i2c_read_bytes (SRF10_CONFIG_ADDRESS, SRF10_RANGE, value, 2) != I2C_OK) {
    return ERROR_SRF10_MEASURE_READING_FAILED;
  }

  /* Convert to the value */
  *distance = ((uint32_t)value[0] << 8) | ((uint32_t)value[1]);
  
  /* All is ok */
  return SRF10_OK;
}
