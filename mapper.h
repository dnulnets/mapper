/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Generic headerfile
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The main header file for the Mapper Robot.
 *
 *****************************************************************************************/
#ifndef _MAPPER_H_
#define _MAPPER_H_

/**
 * \brief Types of messages, ERROR, WARNING and INFO
 **/
enum message_type {MSG_ERROR=1     /**< An ERROR message */
		   , MSG_WARNING=2 /**< A WARNING message */
		   , MSG_INFO=3    /**< An INFO message */
                   };

/*****************************************************************************************
 * Function prototypes
 *****************************************************************************************/
extern void message (enum message_type t, const char *str);
extern void message_value (enum message_type t, const char *str, uint32_t v);
extern void message_string (enum message_type t, const char *str, const char *v);
extern void mdelay(uint32_t ul_dly_ticks);

#endif
