/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Servo I2C Header file
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling i2c communication.
 *
 *****************************************************************************************/
#ifndef _I2C_H_
#define _I2C_H_

/*****************************************************************************************
 * Errors
 *****************************************************************************************/

#define I2C_OK 0 /**< I2C OK */
#define ERROR_I2C_INIT_FAILURE 100 /**< Initialization of the I2C bus failed */
#define ERROR_I2C_WRITE_FAILURE 101 /**< Write communication failure */
#define ERROR_I2C_READ_FAILURE 102 /**< Read communication failure */
#define ERROR_I2C_PROBE_FAILURE 103 /**< TWO probing failed */

/*****************************************************************************************
 * I2C constants
 *****************************************************************************************/

#define I2C_BUS_SPEED 100000 /**< Clock frequency for the i2c bus, 100 kHz */

/*****************************************************************************************
 * Function prototypes
 *****************************************************************************************/
extern uint32_t configure_i2c (void);
extern uint32_t i2c_write_byte (char chip, uint8_t addr, uint8_t b);
extern uint32_t i2c_read_byte (char chip, uint8_t addr, uint8_t *b);
extern uint32_t i2c_read_bytes (char chip, uint8_t addr, uint8_t *buffer, uint32_t length);
extern uint32_t i2c_write_bytes (char chip, uint8_t addr, uint8_t *buffer, uint32_t length);
extern uint32_t i2c_test (char chip);
extern void scan_i2c (void);

#endif
