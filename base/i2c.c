/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot I2C Functions
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The functionality for handling i2c communication on the Arduino Due PA17 and PA18.
 *
 *****************************************************************************************/
#include "asf.h"
#include "stdio_serial.h"
#include "twi.h"

/*****************************************************************************************
 * Local headerfiles
 *****************************************************************************************/
#include "mapper.h"
#include "i2c.h"

/**
 * \brief Configures the i2c interface for the arduino due.
 *
 * Supports only the I2C bus at PA17 and PA18, i.e. TWI0 and sets it up as a master
 * at I2C_BUS_SPEED.
 *
 * \returns Zero on success or failure code.
 **/
uint32_t configure_i2c ()
{
  uint32_t ret;

  message (MSG_INFO, "I2C: Setup and configuration");
  
  /* Setup the I2C bus port PA17 and PA18 on the arduino due */
  ioport_set_port_mode(IOPORT_PIOA, PIO_PA17A_TWD0, IOPORT_MODE_MUX_A);
  ioport_disable_port(IOPORT_PIOA, PIO_PA17A_TWD0);
  
  ioport_set_port_mode(IOPORT_PIOA, PIO_PA18A_TWCK0, IOPORT_MODE_MUX_A);
  ioport_disable_port(IOPORT_PIOA, PIO_PA18A_TWCK0);

  /* Enable the clock to the TWI bus */
  message (MSG_INFO, "I2C: Enable peripheral clock");
  ret = pmc_enable_periph_clk (ID_TWI0);
  if (ret != 0) {
    message_value (MSG_ERROR, "I2C: pmc_enable_periph_clk failed ", ret);
    return ERROR_I2C_INIT_FAILURE;
  }

  /* Set the options */
  twi_options_t opt;
  opt.master_clk = sysclk_get_cpu_hz();
  opt.speed = I2C_BUS_SPEED;
  opt.smbus = 0;
  opt.chip = 0;
  message (MSG_INFO, "I2C: TWI Master init");
  ret = twi_master_init (TWI0, &opt);
  if (ret != TWI_SUCCESS) {
    message_value (MSG_ERROR, "I2C: twi_master_init failed ", ret);
    return ERROR_I2C_INIT_FAILURE;
  }

  /* All went well */
  message (MSG_INFO, "I2C: Setup and configuration completed");  
  return I2C_OK;
}

/**
 * \brief Writes a byte to an address on a specific device.
 *
 * Writes a byte to the address on a given chip on the i2c bus.
 *
 * \param [in] chip I2C address to the device
 * \param [in] addr The address or command to the device
 * \param [in] b The byte to write
 * \return I2C_OK if everything went ok
 **/
uint32_t i2c_write_byte (char chip, uint8_t addr, uint8_t b)
{
  twi_packet_t packet_write;
  packet_write.addr[0] = addr;
  packet_write.addr_length = 1;
  packet_write.chip = chip;
  packet_write.buffer = &b;
  packet_write.length = 1;
  if (twi_master_write (TWI0, &packet_write) != TWI_SUCCESS)
    return ERROR_I2C_WRITE_FAILURE;
  else
    return I2C_OK;
}

/**
 * \brief Writes a sequence of bytes to an address on a specific device.
 *
 * Writes a sequence of bytes to the address on a given device on the i2c bus.
 *
 * \param [in] chip I2C address to the device
 * \param [in] addr The address or command to the device
 * \param [in] b The byte sequence
 * \param [in] length The length of the byte sequence
 * \return TWI_SUCCESS if everything went ok, see twi documentation.
 **/
uint32_t i2c_write_bytes (char chip, uint8_t addr, uint8_t *b, uint32_t length)
{
  twi_packet_t packet_write;
  packet_write.addr[0] = addr;
  packet_write.addr_length = 1;
  packet_write.chip = chip;
  packet_write.buffer = b;
  packet_write.length = length;
  if (twi_master_write (TWI0, &packet_write) != TWI_SUCCESS)
    return ERROR_I2C_WRITE_FAILURE;
  else
    return I2C_OK;
}

/**
 * \brief Reads a byte from an address on a given device on the i2c bus
 *
 * Reads a byte from an address on a given device on the i2c bus.
 *
 * \param [in] chip I2C address to the device
 * \param [in] addr The address or command to the device
 * \param [out] b Pointer to the where the byte is to be stored
 * \return TWI_SUCCESS if everything went ok, see twi documentation.
 **/
uint32_t i2c_read_byte (char chip, uint8_t addr, uint8_t *b)
{
  twi_packet_t packet_write;
  packet_write.addr[0] = addr;
  packet_write.addr_length = 1;
  packet_write.chip = chip;
  packet_write.buffer = b;
  packet_write.length = 1;
  if (twi_master_read (TWI0, &packet_write) != TWI_SUCCESS)
    return ERROR_I2C_READ_FAILURE;
  else
    return I2C_OK;    
}

/**
 * \brief Reads a sequence of bytes from an address on a specific device.
 *
 * Reads a sequence of bytes to the address on a given device on the i2c bus.
 *
 * \param [in] chip I2C address to the device
 * \param [in] addr The address or command to the device
 * \param [out] b The byte buffer
 * \param [in] length The length of the byte buffer
 * \return TWI_SUCCESS if everything went ok, see twi documentation.
 **/
uint32_t i2c_read_bytes (char chip, uint8_t addr, uint8_t *b, uint32_t length)
{
  twi_packet_t packet_write;
  packet_write.addr[0] = addr;
  packet_write.addr_length = 1;
  packet_write.chip = chip;
  packet_write.buffer = b;
  packet_write.length = length;
  if (twi_master_read (TWI0, &packet_write) != TWI_SUCCESS)
    return ERROR_I2C_READ_FAILURE;
  else
    return I2C_OK;    
}

/**
 * \brief Probes the I2C bus for a device on the given address.
 *
 * Checks to see that the device on the given address is active and responding.
 *
 * \param [in] chip Address of the device
 * \return TWI_SUCCESS if the device is present
 **/
uint32_t i2c_test (char chip)
{
  if (twi_probe (TWI0, chip) != TWI_SUCCESS)
    return ERROR_I2C_PROBE_FAILURE;
  else
    return I2C_OK;  
}

/**
 * \brief Scans the i2c bus for devices with address between 1-127
 *
 * Scans the i2c bus for all the devices between 1 and 127. If it finds one it will print
 * out the device address.
 **/
void scan_i2c (void)

{
  uint8_t slave_address;
  uint32_t ret;

  printf ("Scanninig for 7-bits I2C devices\n\r");
  /* Loop through all addresses */
  for (slave_address = 0x08; slave_address<=0x77; slave_address++) {

    /* Probe for the device */
    ret = twi_probe (TWI0, slave_address);
    if (ret == TWI_SUCCESS) {
      printf ("Found i2c device att address %x\n\r", (uint16_t)slave_address);     
    }
    
  }
}
