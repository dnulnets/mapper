/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Clock configuration for the Arduino Due board
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * Sets up the correct clock frequencies, dividers and sources for the Arduino Due Board.
 *
 *****************************************************************************************/
#ifndef CONF_CLOCK_H_INCLUDED
#define CONF_CLOCK_H_INCLUDED

#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_PLLACK
#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_2

#define CONFIG_PLL0_SOURCE          PLL_SRC_MAINCK_XTAL
#define CONFIG_PLL0_MUL             14
#define CONFIG_PLL0_DIV             1

#endif /* CONF_CLOCK_H_INCLUDED */
