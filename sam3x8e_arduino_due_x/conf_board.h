/**************************************************************************************//**
 * \file
 *
 * \brief Mapper Robot Board configuration
 *
 * \author Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 * \copyright Copyright (C) 2017 Tomas Stenlund, Sundsvall, Sweden, tomas.stenlund@telia.com
 *
 * \copyright MIT License, see LICENSE file in project root directory.
 *
 * The defines for activating the correct drivers, functions and modules in Atmels ASF
 * framework.
 *
 *****************************************************************************************/
#ifndef CONF_BOARD_H_INCLUDED
#define CONF_BOARD_H_INCLUDED

#define CONF_BOARD_UART_CONSOLE

#endif /* CONF_BOARD_H_INCLUDED */
